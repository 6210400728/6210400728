# 6210400728

Desktop Application สำหรับการบันทึกการรับ/ส่งจดหมาย เอกสาร หรือพัสดุ สำหรับเจ้าหน้าที่ส่วนกลางที่ดูแลห้องพักอาศัยในคอนโด
  - วิธีการติดตั้งหรือรันโปรแกรม
  - Directory
  - สิ่งที่พัฒนาในแต่ละสัปดาห์
  - วิธีใช้งานโปรแกรมเบื้องต้น

##### วิธีการติดตั้งหรือรันโปรแกรม
1) double click ที่ไฟล์ชื่อ 6210400728.jar ( JAR FILE) 
2) เปิด terminal เข้าไปที่ directory ที่เป็นไฟล์ แล้วใช้คำสั่ง java -jar 6210400728-jar.jar

##### Directory
 **database**  -> เก็บไฟล์ csv สำหรับเขียนไฟล์ และอ่านไฟล์
- admin.csv  -> username,password ของเจ้าหน้าที่ส่วนกลาง
- officer.csv -> ข้อมูลนิติบุคคล
- resident.csv -> ข้อมูลผู้อยู่อาศัย
- room.csv -> ข้อมูลห้องพัก
- letter.csv -> ข้อมูลจดหมาย
- papers.csv -> ข้อมูลเอกสาร
- parcel.csv -> ข้อมูลพัสดุ

**controller**
- ควบคุมการทำงานของ fxml เชื่อมต่อกับการทำงานของ GUI

**model**
- การจัดการและคำนวณข้อมูลของโปรแกรม 

**services**
- การจัดการข้อมูลภายนอกของโปรแกรม

**resources**
- แสดงผลสำหรับผู้ใช้งาน

##### สิ่งที่พัฒนาในแต่ละสัปดาห์
**22-09-2020**
* [a481cf0](https://bitbucket.org/6210400728/6210400728/commits/a481cf09617fb514830bce383b57835df4876b5d)
  - **add maven to project**
    - เพิ่ม maven
 
**23-09-2020**
* [3f7cc87](https://bitbucket.org/6210400728/6210400728/commits/3f7cc87e174e144931ed71e2f9a74ec0c347e93d)
  - **connect pages**
    - เพิ่มการเชื่อมหน้า
* [0f0d9ed](https://bitbucket.org/6210400728/6210400728/commits/0f0d9ed7c841b0c8207f2e8bf97c6f7eac54d1e5)
    - **connect Menu Button**
        - เชื่อมปุ่มโดยใช้ menu button ในการเลือกหน้าแรก

**28-09-2020**
* [fa3ef8b](https://bitbucket.org/6210400728/6210400728/commits/fa3ef8b6b51786ddeeb5a057121b3633e17c1bad)
    - **add Official in project**
        - เพิ่มหน้า fxml และเชื่อมหน้า

**30-09-2020**
* [00b1a8a](https://bitbucket.org/6210400728/6210400728/commits/00b1a8a29d7f4ca190d868272e68cda95b818073)
    - **add interface to project**
        - เพิ่ม class เพิ่มจดหมาย เอกสาร พัสดุ และใช้ interface เพิ่มข้อมูล วัน ,เดือน, ปี ,ตึก ,ชั้น, ห้อง
        - เพิ่ม class admin และ officer ใช้ interface ckeck สำหรับ username, password
        - เพิ่ม fxml ของหน้า login ของ officer และ admin 
        - เพิ่ม fxml สำหรับ officer เพื่อเลือกข้อมูล

**05-10-2020**
* [c38b4b7](https://bitbucket.org/6210400728/6210400728/commits/c38b4b7ca2a770ced165d489a7357fc0ff407334)
    - **read file add write file to project**
        - เพิ่มการอ่านไฟล์ของ admin จากไฟล์ admin.csv

**15-10-2020**
* [d71be88](https://bitbucket.org/6210400728/6210400728/commits/d71be8877af8745c7157d4e4abf542b43e9bfbef)
    - **add fxml**
        - เพิ่มหน้า fxml

**17-10-2020**
* [53e8eb7](https://bitbucket.org/6210400728/6210400728/commits/53e8eb7b80964fe3d1b2c0cb0d5849ddbb36a92e)
    - **edit fxml**
        - แก้ไขหน้า fxml
    

**18-10-2020**
* [60ec71e](https://bitbucket.org/6210400728/6210400728/commits/60ec71ef4a9bc1afa37f00d90e8478183bbf5c8e)
    - **add ChooseForAdmin and SignUpForOfficer**
        - เพิ่มตัวเลือกสำหรับ addmin และสมัคร officer(นิติบุคคล)
* [a89f144](https://bitbucket.org/6210400728/6210400728/commits/a89f144c52d4f78854bb139a0e8b0b852f7a460e)
    - **add LoginForOfficer and SelectData**
        - เพิ่มการใช้งานของ officer(นิติบุคคล) เพื่อเพิ่มข้อมูลของลูกบ้าน และรับพัสดุ

* [f239d1](https://bitbucket.org/6210400728/6210400728/commits/cf239d1b129d48484d68322281efbe31da24f6a1)
    - **add addParcel**
        - เพิ่ม class รับจดหมาย(แต่ยังไม่สามารถรับได้) และเพิ่มห้อง

**20-10-2020**
* [8575161](https://bitbucket.org/6210400728/6210400728/commits/8575161d1aebb41e4dde7b55e6a0f1e7b5178830)
    - **edit package**
        - แก้ไขแพ็คเกจ ให้มีการโยง path ที่ถูกต้อง และนำไฟล์ใส่ลงไปในแพ็คเกจให้เป็นระเบียบแยกเป็นประเภทของไฟล์งาน
    - **edit file**
        - แก้ไขการสมัครให้ officer
        - แก้ไขการอ่านเขียนไฟล์ ให้มี interface ในการใช้งานเพื่อง่ายต่อการนำไปใช้งานต่อไป

**21-10-2020**
* [c5b2273](https://bitbucket.org/6210400728/6210400728/commits/c5b2273f1a5c0bf5acf26f4d17537b66dbe39805)
    - **add tableOfficer**
        - เพิ่มตารางแสดงเวลาการทำงานของ officer 
    
* [1192559](https://bitbucket.org/6210400728/6210400728/commits/119255951b7490d7cd3ef58afdd682480e32e9ba)
    - **create README**
        - เพิ่มเขียน README

**22-10-2020**
* [42e43f1](https://bitbucket.org/6210400728/6210400728/commits/42e43f1286e4b664857cd278ccd0478e77d042d7)
    - **add room**
        - เพิ่มห้องในคอนโด 
        - มีตึกให้ 2 ตึก คือ ตึก A และ B
        - มีชั้นให้เลือก 12 ชั้น คือ 01 - 12
        - มีห้องให้เลือก 16 ห้อง คือ 01 - 16
        - มีขนาดให้เลือก 3 ขนาด คือ 30 Square meters เพิ่มผู้พักอาศัยได้ 2 คน
          40 Square meters เพิ่มผู้พักอาศัยได้ 3 คน และ55 Square meters เพิ่มผู้พักอาศัยได้ 5 คน

**25-10-2020**
* [ac4bcac](https://bitbucket.org/6210400728/6210400728/commits/ac4bcace280ad72a07b63af4ddc9bf7f4ea5a1e5)
    - **add Resident**
        - เพิ่มลูกบ้านเข้าในห้องที่สร้างขึ้นมาแล้ว และจะไม่สามารถเพิ่มได้เมื่อห้องนั้นมีคนเข้าพักเต็มแล้วตามขนาดของห้องพัก
    - **remove Resident**
        - ลบลูกบ้านออกจากห้องได้
    - **view Resident**
        - ดูข้อมูลลูกบ้านในห้องได้
    - **change for admin and officer**
        - เปลี่ยนรหัสผ่านของผู้ดูแลระบบ และเจ้าหน้าที่ส่วนกลางได้
        
**28-10-2020**
* [788e60b](https://bitbucket.org/6210400728/6210400728/commits/788e60bee4cb62f3eb2b3f916ef5f8f4f1e05b63)
    - **add mailbox**
        - เพิ่มเขียน mail box แบ่งเป็น 3 ประเภท ได้แก่ จดหมาย เอกสาร พัสดุ
    - **edit textField-> comboBox**
        - แก้ไขช่องการเลือกห้องจากให้ใส่ห้องเป็น เลือกห้องจากการอ่านไฟล์ resident.csv
    - **received mailbox**
        - รับ mail box และเปลี่ยนสถานะจาก impport เป็น export 
    - **edit name fxml->snack case**
        - แก้ไขชื่อของไฟล์ fxml ให้เป็นชื่อ snack case
        
**29-10-2020**
* [01c021d](https://bitbucket.org/6210400728/6210400728/commits/01c021d4ff8256c628ee813e69cc20fd99056767)
    - **add sign up for resident**
        - เพิ่มการลงทะเบียนสำหรับลูกบ้านที่มีชื่ออยู่ในห้อง ข้อมูลที่กรอกต้องถูกต้องและตรงกับข้อมูลในไฟล์ resident.csv หากไม่ตรงกันไม่สามารถลงทะเบียนได้
        
**01-11-2020**
* [aace769](https://bitbucket.org/6210400728/6210400728/commits/aace7699ad597747dcc8cfc2b39f353bc042baea)
    - **show mail box for resident**
        - แสดงข้อมูล mail box ของลูกบ้านคนนั้นตามที่ login เข้ามา
        
**22-11-2020**
* [40e3be3](https://bitbucket.org/6210400728/6210400728/commits/40e3be3a2ca51d63ad9dfc9d793b0d1273734219)
    - **edit1 polymorphism and validate password**
        - เพิ่ม polymorphism 
            1) AddMailController -> ใช้ method setMailData ได้ 3 รูปแบบ ตามลักษณะการเลือกของผู้ใช้งาน ได้แก่ จดหมาย, เอกสาร และพัสดุ
            2) ExportMailboxController -> ใช้ method getMailData โดยเอาข้อมูลจากไฟล์มาเก็บไว้ในตัวแปร mailList เพื่อนำไปแสดงให้เจ้าหน้าที่ส่วนกลางทราบ ดูได้ 3 รูปบบ ตามลักษณะการเลือกของผู้ใช้งาน ได้แก่ จดหมาย, เอกสาร และพัสดุ
            3) ShowMailBoxController -> ใช้ method getMailData ในการแสดงข้อมูลของ mailbox ให้เจ้าหน้าที่ส่วนกลางทราบ ดูได้ 3 รูปบบ ตามลักษณะการเลือกของผู้ใช้งาน ได้แก่ จดหมาย, เอกสาร และพัสดุ
            4) ShowMailResident -> ใช้ method getMailData ในการแสดงข้อมูลของ mailbox ให้ลูกบ้านที่ login เข้ามาดูได้ 3 รูปบบ ตามลักษณะการเลือกของผู้ใช้งาน ได้แก่ จดหมาย, เอกสาร และพัสดุ
        - แก้ไข validate password 
            1) ChangePasswordAdminController
            2) ChangePasswordOfficerController
            
**27-11-2020**
* [fe2b6be](https://bitbucket.org/6210400728/6210400728/commits/fe2b6beb660b14e7b0459056f07515cb9589e2bb)
    - **edit2 แยก Arraylist ของ mail,room,resident,officer ใน classใหม่ และแก้ไข pdf**
        - แยก arraylist เพิ่ม 4 class
            1) MailList
            2) OfficerList
            3) ResidentList
            4) RoomList
        - แก้ไข pdf
            1) เพิ่มหัวข้อตัวอย่างผู้ใช้งาน
            2) แก้ไขหน้าให้เป็นระเบียบเรียบร้อย

##### วิธีใช้งานโปรแกรมเบื้องต้น
- คลิก "menu" เพื่อเลือก item สำหรับไปหน้าถัดไป
    - login สำหรับลูกบ้านที่มี username และ password แล้ว เข้าสู่ระบบเพื่อดูข้อมูลพัสดุ
    - sign up สำหรับลูกบ้านที่ยังไม่ได้สมัคร usernaame และต้องมีข้อมูลอยู่ในระบบโดยผ่านการกรอกจาก officer 
    - officer สำหรับเจ้าหน้าที่ส่วนกลาง(officer) และ ผู้ดูแลระบบ(admin)
    - about me ดูข้อมูลของผู้จัดทำ
- เจ้าหน้าที่ส่วนกลาง(officer)
    - username และ password ต้องให้เจ้าหน้าที่ส่วนกลางสร้างให้
    - เพิ่มห้องในคอนโด
    - เพิ่มผู้พักอาศัยในคอนโด
- ผู้ดูแลระบบ(admin)
    - username : admin
    - password : 1234
    - เพิ่มเจ้าหน้าที่ส่วนกลาง
    - ดูการทำงานของเจ้าหน้าที่ส่วนกลางว่าใช้งสนล่าสุดเมื่อไร



