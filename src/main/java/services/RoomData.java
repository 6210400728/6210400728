package services;

import model.RoomList;

public interface RoomData {
    RoomList getRoomData();

    void setRoomData(RoomList room);
}
