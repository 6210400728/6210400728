package services;

import model.Resident;
import model.ResidentList;

import java.util.ArrayList;

public interface ResidentData {
    ResidentList getResidentData();

    void setResidentData(ArrayList<Resident> resident);
}
