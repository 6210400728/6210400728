package services;

import model.Mail;
import model.MailList;

public interface MailData {
    MailList getMailData();
    void setMailData(MailList mail);
}
