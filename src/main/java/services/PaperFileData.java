package services;

import model.Mail;
import model.MailList;
import model.Papers;

import java.io.*;

public class PaperFileData implements MailData{
    private String fileDirectoryName;
    private String fileName;
    private MailList mailList;


    public PaperFileData(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }


    private void readPaperData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Mail newMail = new Papers(data[0], data[1], data[2],data[3], data[4], data[6], data[7]);
            newMail.setRoom(data[5]);
            mailList.addMail(newMail);
        }
        reader.close();
    }

    public MailList getMailData() {
        try {
            mailList = new MailList();
            readPaperData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return mailList;
    }

    public void setMailData(MailList list) {

        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Mail mail1 : list.getMail()) {
                Papers paper = (Papers) mail1;
                String line = mail1.getSender() + ","
                        + mail1.getReceiver() + ","
                        + mail1.getSize() + ","
                        + paper.getImportant() + ","
                        + mail1.getTime() + ","
                        + mail1.getRoom()+","
                        + mail1.getStatus() + ","
                        + mail1.getReceiveBy();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }

    }
}
