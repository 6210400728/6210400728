package services;

import model.Resident;
import model.ResidentList;

import java.io.*;
import java.util.ArrayList;

public class ResidentFileData implements ResidentData {

    private String fileDirectoryName;
    private String fileName;
    private ResidentList residentList;

    public ResidentFileData(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Resident newResident = new Resident(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]);
            residentList.add(newResident);
        }
        reader.close();
    }


    @Override
    public ResidentList getResidentData() {
        try {
            residentList = new ResidentList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return residentList;
    }

    @Override
    public void setResidentData(ArrayList<Resident> resident) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Resident resident1 : residentList.getResidents()) {
                String line = resident1.getName() + ","
                        + resident1.getSurName() + ","
                        + resident1.getPhone() + ","
                        + resident1.getBuilding() + ","
                        + resident1.getFloor() + ","
                        + resident1.getRoom() + ","
                        + resident1.getSize() + ","
                        + resident1.getUserName() + ","
                        + resident1.getPassword() ;
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }

    }


}
