package services;


import model.Officer;
import model.OfficerList;

public interface OfficerData {
    OfficerList getOfficerData();

    void setOfficerData(OfficerList officer);
}
