package services;


import model.Officer;
import model.OfficerList;

import java.io.*;

public class OfficerFileData implements OfficerData {

    private String fileDirectoryName;
    private String fileName;
    private OfficerList officerList;

    public OfficerFileData(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Officer newOfficer = new Officer(data[0], data[1], data[2], data[3], data[4], data[5]);
            officerList.addOfficer(newOfficer);
        }
        reader.close();
    }

    @Override
    public OfficerList getOfficerData() {
        try {
            officerList = new OfficerList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return officerList;
    }

    @Override
    public void setOfficerData (OfficerList list) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Officer acc : list.getOfficersList()) {
                String line = acc.getName() + ","
                        + acc.getSurName() + ","
                        + acc.getPhone() + ","
                        + acc.getUserName() + ","
                        + acc.getPassword() + ","
                        + acc.getTime();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
