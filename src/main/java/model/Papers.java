package model;

import java.util.ArrayList;
import java.util.Date;

public class Papers extends Mail {
    private String important;

    public Papers(String sender, String receiver, String size,  String important, String time,String status, String receiveBy) {
        super(sender, receiver, size, time, status, receiveBy);
        this.important = important;
    }


    public String getImportant() {
        return important;
    }

    public void setImportant(String important) {
        this.important = important;
    }

}
