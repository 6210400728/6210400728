package model;

import java.util.ArrayList;

public class OfficerList {
    private ArrayList<Officer> officersList;

    public OfficerList() {
        officersList = new ArrayList<>();
    }

    public ArrayList<Officer> getOfficersList() {
        return officersList;
    }

    public void addOfficer(Officer officer){
        officersList.add(officer);
    }

    public Officer checkAndGetOfficer(String username, String password){
        for (Officer officer : officersList) {
            if(username.equals(officer.getUserName()) && password.equals(officer.getPassword())){
                return officer;
            }
        }
        return null;
    }
}
