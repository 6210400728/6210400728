package model;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class Room {
    private String build;
    private String floor;
    private String numRoom;
    private String size;
    private int maxResident;
    private int countResident;
    private String room;
    private Button viewBtn = new Button();

    public Room(String build, String floor, String numRoom, String size, int maxResident, int countResident) {
        this.build = build;
        this.floor = floor;
        this.numRoom = numRoom;
        this.size = size;
        this.maxResident = maxResident;
        this.countResident = countResident;
        this.room = numRoom;
    }



    public Button getViewBtn() {
        return viewBtn;
    }


    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getNumRoom() {
        return room;
    }

    public void setNumRoom(String numRoom) {
        this.numRoom = numRoom;
    }

    public String getSize() {
        return size;
    }

    public int getMaxResident() {
        return maxResident;
    }

    public int getCountResident() {
        return countResident;
    }

    public String getRoom() {
        return room;
    }

    public void setCountResident(int countResident) {
        this.countResident = countResident;
    }


}
