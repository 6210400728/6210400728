package model;

import java.util.ArrayList;

public class ResidentList {

    private ArrayList<Resident> residents;

    public ResidentList(){
        residents=new ArrayList<>();
    }

    public void add(Resident resident){
        residents.add(resident);
    }

    public ArrayList<Resident> getResidents() {
        return residents;
    }
    public void remove(Resident resident){
        residents.remove(resident);
    }

    public Resident checkAndGetResident(String username, String password){
        for (Resident resident : residents) {
            if(username.equals(resident.getUserName()) && password.equals(resident.getPassword())){
                return resident;
            }
        }
        return null;
    }

    public ArrayList<Resident> room1(){
        return residents;
    }
}
