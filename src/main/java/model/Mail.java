package model;

import java.util.ArrayList;

public class Mail {
    private String sender;
    private String receiver;
    private String size;
    private String time;
    private String status;
    private String receiveBy;
    private String room;
    private MailList mailList;

    public Mail(String sender, String receiver, String size, String time, String status, String receiveBy) {
        this.sender = sender;
        this.receiver = receiver;
        this.size = size;
        this.time = time;
        this.status = status;
        this.receiveBy = receiveBy;
    }


    public void setRoom(String room) {
        this.room = room;
    }

    public String getRoom() {
        return room;
    }


    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiveBy() {
        return receiveBy;
    }

    public void setReceiveBy(String receiveBy) {
        this.receiveBy = receiveBy;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




    @Override
    public String toString() {
        return "Mail{" +
                "sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", size='" + size + '\'' +
                ", time='" + time + '\'' +
                ", status='" + status + '\'' +
                ", receiveBy='" + receiveBy + '\'' +
                ", mailList=" + mailList +
                '}';
    }
}
