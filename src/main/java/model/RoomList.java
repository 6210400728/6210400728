package model;

import java.util.ArrayList;

public class RoomList {
    private ArrayList<Room> roomList;
    public void addRoom(Room newRoom){
        roomList.add(newRoom);
    }

    public RoomList() {
        roomList = new ArrayList<>();
    }

    public ArrayList<Room> getRoomList() {
        return roomList;
    }

    public boolean checkAndAddRoom(Room room1){
        for (Room newRoom:roomList) {
            if(newRoom.getRoom().equals(room1.getRoom())){
                return false;
            }
        }
        roomList.add(room1);
        return true;
    }

    public void deleteCount(Room roomCheck){
        for (Room room:roomList) {
            if(room.getNumRoom().equals(roomCheck.getNumRoom())){
                room.setCountResident(room.getCountResident()-1);
            }
        }
    }

    public void addCount(Room roomCheck){
        for (Room room:roomList) {
            if(room.getNumRoom().equals(roomCheck.getNumRoom())){
                room.setCountResident(room.getCountResident()+1);
            }
        }
    }
}
