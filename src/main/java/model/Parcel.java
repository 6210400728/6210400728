package model;

import java.util.ArrayList;
import java.util.Date;

public class Parcel extends Mail {
    private String shipping;
    private String tracking;

    public Parcel(String sender, String receiver, String size, String shipping, String tracking,String time,  String status, String receiveBy) {
        super(sender, receiver, size, time, status, receiveBy);
        this.shipping = shipping;
        this.tracking = tracking;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }
}
