package model;

import java.util.ArrayList;

public class Condo {
    private ArrayList<Room> rooms = new ArrayList<>();

    public Room findRoom(String roomNumber){
        for (Room room : rooms) {
            if(room.getNumRoom().equals(roomNumber)){
                return room;
            }
        }throw new IllegalArgumentException("This number of room is not correct.");
    }

}
