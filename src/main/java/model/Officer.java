package model;

import java.util.ArrayList;

public class Officer extends User{

    private String time;

    public Officer(String name, String surName, String phone, String userName, String password, String time) {
        super(name, surName, phone, userName, password);
        this.time = time;

    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



}
