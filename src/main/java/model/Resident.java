package model;

import java.util.ArrayList;

public class Resident extends User implements Cloneable{
    private String building;
    private String floor;
    private String size;
    private String room;

    public Resident(String name, String surName, String phone, String building, String floor, String room, String size,String userName, String password) {
        super(name, surName, phone, userName, password);
        this.building = building;
        this.floor = floor;
        this.size = size;
        this.room = room;
    }


    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getBuilding() {
        return building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    @Override
    public String toString() {
        return "Resident{" +"name='" + getName() + '\'' +
                ", surName='" + getSurName() + '\'' +
                ", phone='" + getPhone() + '\'' +
                ", userName='" + getUserName() + '\'' +
                ", password='" + getPassword() + '\'' +
                "building='" + building + '\'' +
                ", floor='" + floor + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    public boolean check(String userAdministrator, String passwordAdministrator) {
        if(userAdministrator.equals(this.getUserName()) && passwordAdministrator.equals(this.getPassword())){
            return true;
        }
        return false;
    }

    public boolean checkResident(String name, String surname, String phone, String room) {
        if (name.equals(this.getName()) && surname.equals(this.getSurName()) && phone.equals(this.getPhone()) && room.equals(this.getRoom())) {
            return true;
        }
        return false;
    }

}
