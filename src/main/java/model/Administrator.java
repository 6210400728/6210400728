package model;

public class Administrator{
    private String userAdministrator;
    private String passwordAdministrator;

    public Administrator() {
        this.userAdministrator = "admin";
        this.passwordAdministrator = "1234";
    }
        public Administrator(String userAdministrator, String passwordAdministrator) {
            this.userAdministrator = userAdministrator;
            this.passwordAdministrator = passwordAdministrator;
        }

        public String getUserAdministrator() {
            return userAdministrator;
        }
        public String getPasswordAdministrator() {
        return passwordAdministrator;
    }

    public void setPasswordAdministrator(String passwordAdministrator) {
        this.passwordAdministrator = passwordAdministrator;
    }

    public boolean check(String userAdministrator, String passwordAdministrator) {
        if(userAdministrator.equals(this.userAdministrator) && passwordAdministrator.equals(this.passwordAdministrator)){
            return true;
        }
        return false;
    }


}
