package model;

import java.util.ArrayList;

public class MailList {
    private ArrayList<Mail> mailList;

    public MailList() {
        mailList = new ArrayList<>();
    }

    public ArrayList<Mail> getLetters() {
        return mailList;
    }
    public void addMail(Mail mail) {
        mailList.add(mail);
    }

    public ArrayList<Mail> getMail() {
        return mailList;
    }

    public void setMail(ArrayList<Mail> mail) {
        this.mailList = mail;
    }

    public MailList getImportItems(){
        MailList importList = new MailList();
        for(Mail item : mailList){
            if(item.getStatus().equals("import")){
                importList.addMail(item);
            }
        }
        return importList;
    }

    public MailList getExportItems(){
        MailList exportList = new MailList();
        for(Mail item : mailList){
            if(item.getStatus().equals("export")){
                exportList.addMail(item);
            }
        }
        return exportList;
    }

}
