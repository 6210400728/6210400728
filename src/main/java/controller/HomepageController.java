package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;


public class HomepageController {
    @FXML MenuButton menuMnBtn;
    @FXML MenuItem loginBtn;
    @FXML MenuItem signupBtn;
    @FXML MenuItem officialBtn;
    @FXML MenuItem aboutmeBtn;
    @FXML MenuItem howto;
    @FXML Button confirmBtn;

    private String selectedOption = "";


    @FXML public void handleConfirmOnAction(ActionEvent event) throws IOException {
        Button ButtonAboutMe = (Button) event.getSource();
        Stage stage = (Stage) ButtonAboutMe.getScene().getWindow();

        if(selectedOption.equals("loginBtn")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            stage.show();
        }
        else if(selectedOption.equals("signupBtn")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sign_up.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            stage.show();
        }
        else if(selectedOption.equals("officialBtn")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/official.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            stage.show();
        }
        else if(selectedOption.equals("aboutmeBtn")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/about_me.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            stage.show();
        }
        else if(selectedOption.equals("howto")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/how_to_use.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            stage.show();
        }


    }

    @FXML public void handleLoginOnAction(ActionEvent event) throws IOException{
        MenuItem menuItemLogin = (MenuItem) event.getSource();
        selectedOption = menuItemLogin.getId();
        menuMnBtn.setText(menuItemLogin.getText());
    }

    @FXML public void handleSignUpOnAction(ActionEvent event) throws IOException{
        MenuItem menuItemSignUp = (MenuItem) event.getSource();
        selectedOption = menuItemSignUp.getId();
        menuMnBtn.setText(menuItemSignUp.getText());
    }

    @FXML public void handleOfficialOnAction(ActionEvent event) throws IOException{
        MenuItem menuItemOfficial = (MenuItem) event.getSource();
        selectedOption = menuItemOfficial.getId();
        menuMnBtn.setText(menuItemOfficial.getText());
    }

    @FXML public void handleAboutMeOnAction(ActionEvent event) throws IOException{
        MenuItem menuItemAboutMe = (MenuItem) event.getSource();
        selectedOption = menuItemAboutMe.getId();
        menuMnBtn.setText(menuItemAboutMe.getText());
    }
    @FXML public void handleHowToOnAction(ActionEvent event) throws IOException{
        MenuItem menuItemHowTo = (MenuItem) event.getSource();
        selectedOption = menuItemHowTo.getId();
        menuMnBtn.setText(menuItemHowTo.getText());
    }


}
