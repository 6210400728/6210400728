package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;
import services.*;

import java.io.IOException;
import java.util.ArrayList;

public class ShowMailResident {
    @FXML
    Button backBtn;
    @FXML
    TableView<Mail> mailTable;
    private ObservableList<Mail> mailObservableList;
    private MailData mailFile;
    private Mail mail, exportList;
    private MailList mailList;
    private Mail papersList;
    private Mail parcelList;
    private String selectType;
    private String selectData;
    private Resident current;
    private RoomFileData roomFile;
    private ArrayList<Room> rooms;

    public void setCurrent(Resident current){
        this.current = current;
    }

    public void setSelectData(String selectData) {

        this.selectData = selectData;
    }

    public void setSelectType(String selectType){
        this.selectType = selectType;
    }

    @FXML public void initialize() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(selectType.equals("papersBtn")){
                    mailFile = new PaperFileData("database", "papers.csv");
                }else if(selectType.equals("letterBtn")){
                    mailFile = new LetterFileData("database", "letter.csv");
                }else if(selectType.equals("parcelBtn")){
                    mailFile = new ParcelFileData("database", "parcel.csv");
                }
                mailList = mailFile.getMailData();
                showData();
            }


        });

    }
    public void showData(){
                    if(selectType.equals("papersBtn")){
                        MailList papers = new MailList();
                        for(Mail mail:mailList.getMail()){
                            if(mail.getRoom().equals(current.getRoom())){
                                papers.addMail(mail);
                            }
                        }
                        mailObservableList = FXCollections.observableArrayList(papers.getMail());
                        mailTable.setItems(mailObservableList);
                        TableColumn date = new TableColumn("Date & Time");
                        TableColumn sender = new TableColumn("Sender");
                        TableColumn receiver = new TableColumn("Receiver");
                        TableColumn size = new TableColumn("Size");
                        TableColumn important = new TableColumn("Priority");
                        TableColumn status = new TableColumn("Status");
                        TableColumn receiveBy = new TableColumn("Received");


                        date.setCellValueFactory(new PropertyValueFactory<Papers,String>("time"));
                        sender.setCellValueFactory(new PropertyValueFactory<Papers,String>("sender"));
                        receiver.setCellValueFactory(new PropertyValueFactory<Papers,String>("receiver"));
                        size.setCellValueFactory(new PropertyValueFactory<Papers,String>("size"));
                        important.setCellValueFactory(new PropertyValueFactory<Papers,String>("important"));
                        status.setCellValueFactory(new PropertyValueFactory<Papers,String>("status"));
                        receiveBy.setCellValueFactory(new PropertyValueFactory<Papers,String>("receiveBy"));

                        mailTable.getColumns().addAll(date, sender,receiver,size,important,status,receiveBy);
                        date.setSortType(TableColumn.SortType.DESCENDING);
                        mailTable.getSortOrder().add(date);
                    }
                    else if (selectType.equals("letterBtn")){
                        MailList letter = new MailList();
                        for(Mail mail: mailList.getMail()){
                            if(mail.getRoom().equals(current.getRoom())){
                                letter.addMail(mail);
                            }
                        }
                        mailObservableList = FXCollections.observableArrayList(letter.getMail());
                        mailTable.setItems(mailObservableList);
                        TableColumn date = new TableColumn("Date & Time");
                        TableColumn sender = new TableColumn("Sender");
                        TableColumn receiver = new TableColumn("Receiver");
                        TableColumn size = new TableColumn("Size");
                        TableColumn status = new TableColumn("Status");
                        TableColumn receiveBy = new TableColumn("Received");

                        date.setCellValueFactory(new PropertyValueFactory<Letter,String>("time"));
                        sender.setCellValueFactory(new PropertyValueFactory<Letter,String>("sender"));
                        receiver.setCellValueFactory(new PropertyValueFactory<Letter,String>("receiver"));
                        size.setCellValueFactory(new PropertyValueFactory<Letter,String>("size"));
                        status.setCellValueFactory(new PropertyValueFactory<Letter,String>("status"));
                        receiveBy.setCellValueFactory(new PropertyValueFactory<Letter,String>("receiveBy"));


                        mailTable.getColumns().addAll(date, sender,receiver,size, status, receiveBy);
                        date.setSortType(TableColumn.SortType.DESCENDING);
                        mailTable.getSortOrder().add(date);
                    }
                    else if (selectType.equals("parcelBtn")){
                        MailList parcel = new MailList();
                        for(Mail mail:mailList.getMail()){
                            if(mail.getRoom().equals(current.getRoom())){
                                parcel.addMail(mail);
                            }
                        }
                        mailObservableList = FXCollections.observableArrayList(parcel.getMail());
                        mailTable.setItems(mailObservableList);
                        TableColumn date = new TableColumn("Date & Time");
                        TableColumn sender = new TableColumn("Sender");
                        TableColumn receiver = new TableColumn("Receiver");
                        TableColumn size = new TableColumn("Size");
                        TableColumn shipping = new TableColumn("Shipping");
                        TableColumn tracking = new TableColumn("Tracking no.");
                        TableColumn status = new TableColumn("Status");
                        TableColumn receiveBy = new TableColumn("Received");

                        date.setCellValueFactory(new PropertyValueFactory<Parcel,String>("time"));
                        sender.setCellValueFactory(new PropertyValueFactory<Parcel,String>("sender"));
                        receiver.setCellValueFactory(new PropertyValueFactory<Parcel,String>("receiver"));
                        size.setCellValueFactory(new PropertyValueFactory<Parcel,String>("size"));
                        shipping.setCellValueFactory(new PropertyValueFactory<Parcel,String>("shipping"));
                        tracking.setCellValueFactory(new PropertyValueFactory<Parcel,String>("tracking"));
                        status.setCellValueFactory(new PropertyValueFactory<Parcel,String>("status"));
                        receiveBy.setCellValueFactory(new PropertyValueFactory<Parcel,String>("receiveBy"));


                        mailTable.getColumns().addAll(date, sender,receiver,size, shipping,tracking,status,receiveBy);
                        date.setSortType(TableColumn.SortType.DESCENDING);
                        mailTable.getSortOrder().add(date);
                    }



    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_before_show_resident.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            ChooseTypeBeforeShowResidentController choose = loader.getController();
            ChooseTypeBeforeShowResidentController back = loader.getController();
            back.setCurrent(current);
            stage.show();
    }
}
