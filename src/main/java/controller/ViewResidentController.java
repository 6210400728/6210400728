package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Officer;
import model.Resident;
import model.ResidentList;
import model.Room;
import services.ResidentData;
import services.ResidentFileData;

import java.io.IOException;
import java.util.ArrayList;

public class ViewResidentController {
    @FXML Button backBtn;
    @FXML TableView<Resident> residentTable;
    private ObservableList<Resident> residentObservableList;
    private ResidentList residentList;
    private ResidentData residentData;
    private Room room;
    private Officer officer;
    @FXML Label officerLabel;

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    public void initialize(){
        residentData = new ResidentFileData("database","resident.csv");
        residentList = residentData.getResidentData();
        Platform.runLater(new Runnable()
        {   @Override public void run(){
            officerLabel.setText("Account officer : "+officer.getUserName());
            showData();
        }
        });
    }
    public void showData(){
        ArrayList<Resident> residentChoose = new ArrayList<>();
        for(Resident resident:residentList.room1()){
            if(resident.getRoom().equals(room.getNumRoom())){
                residentChoose.add(resident);
            }
        }


        residentObservableList= FXCollections.observableArrayList(residentChoose);
        residentTable.setItems(residentObservableList);

        TableColumn name = new TableColumn("Name");
        TableColumn surname = new TableColumn("Surname");
        TableColumn username = new TableColumn("Phone Number");

        name.setCellValueFactory(new PropertyValueFactory<Resident,String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<Resident,String>("surName"));
        username.setCellValueFactory(new PropertyValueFactory<Resident,String>("phone"));


        residentTable.getColumns().addAll( name,surname,username);

    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_room.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        ShowRoomController back = loader.getController();
        back.setOfficer(officer);
        back.setSelectData("viewInfoBtn");
        stage.show();

    }
}
