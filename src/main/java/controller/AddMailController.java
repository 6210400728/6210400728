package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.*;
import services.LetterFileData;
import services.MailData;
import services.PaperFileData;
import services.ParcelFileData;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddMailController {
    private Officer officer;
    private Room room;
    private MailData mailFileData;
    private MailList mailList;
    private String type;

    @FXML Label officerLabel,ship,track,pri;
    @FXML TextField senderTextField;
    @FXML TextField sizeTextField;
    @FXML TextField receiverTextField;
    @FXML TextField shippingTextField;
    @FXML TextField trackingTextField;
    @FXML ComboBox<String> priority;


    public void setOfficer(Officer officer) {
        this.officer = officer;
    }
    public void setRoom(Room room){
        this.room = room;
    }
    public void setType(String type) {
        this.type = type;
    }

    @FXML public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                priority.getItems().addAll("express","secret","standard");
                officerLabel.setText("Account officer : " + officer.getUserName());
                if(type.equals("letter")){
                    shippingTextField.setDisable(true);
                    trackingTextField.setDisable(true);
                    priority.setDisable(true);
                    ship.setDisable(true);
                    track.setDisable(true);
                    pri.setDisable(true);
                    mailFileData = new LetterFileData("database","letter.csv");
                    mailList = mailFileData.getMailData();//polymorphism
                }else if(type.equals("parcel")){
                    priority.setDisable(true);
                    pri.setDisable(true);
                    mailFileData = new ParcelFileData("database","parcel.csv");
                    mailList = mailFileData.getMailData();//polymorphism
                }else if(type.equals("paper")){
                    shippingTextField.setDisable(true);
                    trackingTextField.setDisable(true);
                    ship.setDisable(true);
                    track.setDisable(true);

                    mailFileData = new PaperFileData("database","papers.csv");

                }
                mailList = mailFileData.getMailData();//polymorphism
            }
        });

    }

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException{
        Mail mail=null;
        boolean check = false;
        if(type.equals("parcel")) {
            if (senderTextField.getText().equals("") || receiverTextField.getText().equals("") || sizeTextField.getText().equals("") ||
                    shippingTextField.getText().equals("") || trackingTextField.getText().equals("")) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Mail Box");
                alert.setHeaderText("");
                alert.setContentText("Please fill in all required information.");
                alert.showAndWait();

            }else {
                String date = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
                mail = new Parcel(senderTextField.getText(), receiverTextField.getText(), sizeTextField.getText(), shippingTextField.getText(),
                        trackingTextField.getText(), date, "import", "-");
                mail.setRoom(room.getRoom());
                mail.setTime(date);
                check = true;
            }
        }else if(type.equals("paper")){
            
            if(senderTextField.getText().equals("") || receiverTextField.getText().equals("") || sizeTextField.getText().equals("") || priority.getSelectionModel().getSelectedItem() == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Mail Box");
                alert.setHeaderText("");
                alert.setContentText("Please fill in all required information.");
                alert.showAndWait();
            }
            else {
                String date = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
                mail = new Papers(senderTextField.getText(), receiverTextField.getText(), sizeTextField.getText(), priority.getValue(), date, "import", "-");
                mail.setRoom(room.getRoom());
                mail.setTime(date);
                check = true;
            }
        }else if(type.equals("letter")){
            if(senderTextField.getText().equals("") || receiverTextField.getText().equals("") || sizeTextField.getText().equals("")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Mail Box");
                alert.setHeaderText("");
                alert.setContentText("Please fill in all required information.");
                alert.showAndWait();
            }
            else {
                String date = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
                mail = new Mail(senderTextField.getText(), receiverTextField.getText(), sizeTextField.getText(), date, "import", "-");
                mail.setRoom(room.getRoom());
                mail.setTime(date);
                check = true;
            }
        }
        if(check) {
            mailList.addMail(mail);
            mailFileData.setMailData(mailList);//polymorphism
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Mail box");
            alert.setHeaderText("");
            alert.setContentText("Add parcel complete.");
            alert.showAndWait();

            Button n = (Button) event.getSource();
            Stage stage = (Stage) n.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            ChooseTypeMailBoxController add = loader.getController();
            add.setOfficer(officer);
            stage.show();
        }
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_mailbox.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        ChooseTypeMailBoxController back = loader.getController();
        back.setOfficer(officer);
        stage.show();

    }
}
