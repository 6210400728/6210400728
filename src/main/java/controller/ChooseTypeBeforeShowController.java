package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Officer;
import model.Resident;

import java.io.IOException;

public class ChooseTypeBeforeShowController {
    @FXML Label officerLabel;
    private String selectType = "";
    private Officer officer;
    private String selectData;

    public void setSelectData(String selectData) {
        this.selectData = selectData;
    }
    public void setOfficer(Officer officer) {
        this.officer = officer;
    }
    public void setSelectType(String selectType){
        this.selectType = selectType;
    }

    public void initialize(){
        Platform.runLater(() ->
                officerLabel.setText("Account officer : "+officer.getUserName()));
    }

    @FXML public void handlePapersOnAction(ActionEvent event) throws IOException {
        if(selectData.equals("exportBtn")){
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/export_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "papersBtn";
            selectData = "exportBtn";
            ExportMailboxController papers = loader.getController();
            papers.setSelectType(selectType);
            papers.setSelectData(selectData);
            papers.setOfficer(officer);
            stage.show();
        }
        else if(selectData.equals("viewBtn")){
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "papersBtn";
            selectData = "viewBtn";
            ShowMailBoxController papers = loader.getController();
            papers.setSelectType(selectType);
            papers.setSelectData(selectData);
            papers.setOfficer(officer);
            stage.show();
        }

    }

    @FXML public void handleLetterOnAction(ActionEvent event) throws IOException {
        if(selectData.equals("exportBtn")){
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/export_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "letterBtn";
            selectData = "exportBtn";
            ExportMailboxController letter = loader.getController();
            letter.setSelectData(selectData);
            letter.setSelectType(selectType);
            letter.setOfficer(officer);
            stage.show();
        }
        else if(selectData.equals("viewBtn")){
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "letterBtn";
            selectData = "viewBtn";
            ShowMailBoxController letter = loader.getController();
            letter.setSelectType(selectType);
            letter.setSelectData(selectData);
            letter.setOfficer(officer);
            stage.show();
        }

    }

    @FXML public void handleParcelOnAction(ActionEvent event) throws IOException {
        if(selectData.equals("exportBtn")){
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/export_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "parcelBtn";
            selectData = "exportBtn";
            ExportMailboxController parcel = loader.getController();
            parcel.setSelectType(selectType);
            parcel.setSelectData(selectData);
            parcel.setOfficer(officer);
            stage.show();
        }
        else if(selectData.equals("viewBtn")){
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mailbox.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "parcelBtn";
            selectData = "viewBtn";
            ShowMailBoxController parcel = loader.getController();
            parcel.setSelectType(selectType);
            parcel.setSelectData(selectData);
            parcel.setOfficer(officer);
            stage.show();
        }


    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
            Button b1 = (Button) event.getSource();
            Stage stage = (Stage) b1.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/select_data.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            SelectDataController select = loader.getController();
            select.setOfficer(officer);
            stage.show();

    }

}
