package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Officer;
import model.OfficerList;
import services.OfficerData;
import services.OfficerFileData;

import java.io.IOException;

public class ShowTimeOfficerController {
    @FXML Button backBtn;
    @FXML TableView<Officer> officerTable;
    private ObservableList<Officer> officerObservableList;
    private OfficerList officerList;
    private OfficerData officerData;


    public void initialize(){
        officerList = new OfficerList();
        officerData = new OfficerFileData("database","officer.csv");
        officerList = officerData.getOfficerData();
        Platform.runLater(() -> {
            showData();
        });
    }

    public void showData(){
        officerObservableList = FXCollections.observableArrayList(officerList.getOfficersList());
        officerTable.setItems(officerObservableList);

        TableColumn date = new TableColumn("Date & Time");
        TableColumn name = new TableColumn("Name");
        TableColumn surname = new TableColumn("Surname");
        TableColumn username = new TableColumn("Username");
        TableColumn password = new TableColumn("Password");

        date.setCellValueFactory(new PropertyValueFactory<Officer,String>("time"));
        name.setCellValueFactory(new PropertyValueFactory<Officer,String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<Officer,String>("surName"));
        username.setCellValueFactory(new PropertyValueFactory<Officer,String>("userName"));
        password.setCellValueFactory(new PropertyValueFactory<Officer,String>("password"));

        officerTable.getColumns().addAll(date, name,surname,username,password);
        date.setSortType(TableColumn.SortType.DESCENDING);
        officerTable.getSortOrder().add(date);

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_for_admin.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

}
