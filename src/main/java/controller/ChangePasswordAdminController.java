package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import model.Administrator;

import java.io.*;

public class ChangePasswordAdminController {
    private Administrator admin;
    @FXML PasswordField oldPasswordField;
    @FXML PasswordField newPasswordField;
    @FXML PasswordField confirmPasswordField;
    @FXML Button okBtn;
    @FXML Button backBtn;

    @FXML public void initialize() throws IOException {
        FileReader fileReader = new FileReader("database/admin.csv");
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            admin = new Administrator(data[0],data[1]);
        }
        reader.close();
    }

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {
        if(oldPasswordField.getText().isEmpty() || newPasswordField.getText().isEmpty() || confirmPasswordField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Password");
            alert.setHeaderText("");
            alert.setContentText("Please fill in all required password.");
            alert.showAndWait();
        }else {
            if (oldPasswordField.getText().equals(admin.getPasswordAdministrator())) {
                if (newPasswordField.getText().equals(confirmPasswordField.getText())) {
                    admin.setPasswordAdministrator(newPasswordField.getText());
                    FileWriter fileWriter = null;
                    try {
                        fileWriter = new FileWriter("database/admin.csv");
                        BufferedWriter writer = new BufferedWriter(fileWriter);
                        String line = admin.getUserAdministrator() + "," + admin.getPasswordAdministrator();
                        writer.append(line);
                        writer.newLine();
                        writer.close();

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Password");
                        alert.setHeaderText("");
                        alert.setContentText("Change password complete.");
                        alert.showAndWait();

                        Button o = (Button) event.getSource();
                        Stage stage_homepage = (Stage) o.getScene().getWindow();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_administrator.fxml"));
                        stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                        stage_homepage.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Password");
                    alert.setHeaderText("");
                    alert.setContentText("Password is not correct.");
                    newPasswordField.setText("");
                    confirmPasswordField.setText("");
                    alert.showAndWait();
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Password");
                alert.setHeaderText("");
                alert.setContentText("Password is not correct.");
                oldPasswordField.setText("");
                alert.showAndWait();
            }
        }
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {

        Button h = (Button) event.getSource();
        Stage stage_homepage = (Stage) h.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_administrator.fxml"));
        stage_homepage.setScene(new Scene(loader.load(),800,600));
        stage_homepage.show();
    }



}
