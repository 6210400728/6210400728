package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Officer;
import model.Room;
import model.RoomList;
import services.RoomData;
import services.RoomFileData;

import java.io.IOException;

public class AddRoomController{

    @FXML private ComboBox<String> building;
    @FXML private ComboBox<String> floor;
    @FXML private ComboBox<String> size;
    @FXML private ComboBox<String> roomNum;
    @FXML Label officerLabel;

    private Room room;
    private RoomList roomList;
    private RoomData roomData;
    private Officer officer;

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    public void initialize(){

        Platform.runLater(new Runnable()
        {   @Override public void run(){
            officerLabel.setText("Account officer : "+officer.getUserName());
        }
        });
        building.getItems().addAll("A", "B");
        floor.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12");
        size.getItems().addAll("30 Square meters","40 Square meters","55 Square meters");
        roomNum.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12","12A","14","15","16");
        roomData = new RoomFileData("database","room.csv");
        roomList = roomData.getRoomData();
    }

    @FXML Button okBtn;
    @FXML Button backBtn;


    @FXML public void handleBackOnAction(ActionEvent event) throws IOException{
        roomData = new RoomFileData("database","room.csv");
        roomList = roomData.getRoomData();
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/select_data.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        SelectDataController add = loader.getController();
        add.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {

        if (building.getValue() != null && floor.getValue() != null && roomNum.getValue() != null && size.getValue() != null) {
            if (size.getValue().equals("30 Square meters")) {
                room = new Room(building.getValue(), floor.getValue(),(building.getValue()+floor.getValue()+roomNum.getValue()), size.getValue(), 2, 2);
            } else if (size.getValue().equals("40 Square meters")) {
                room = new Room(building.getValue(), floor.getValue(), (building.getValue()+floor.getValue()+roomNum.getValue()), size.getValue(), 3, 3);
            } else if (size.getValue().equals("55 Square meters")) {
                room = new Room(building.getValue(), floor.getValue(), (building.getValue()+floor.getValue()+roomNum.getValue()), size.getValue(), 5, 5);
            }

            if (roomList.checkAndAddRoom(room)) {
                roomData = new RoomFileData("database", "room.csv");
                roomData.setRoomData(roomList);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Room");
                alert.setHeaderText("");
                alert.setContentText("Create room complete.");
                alert.showAndWait();
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/add_room.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                AddRoomController add = loader.getController();
                add.setOfficer(officer);
                stage.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Room");
                alert.setHeaderText("");
                alert.setContentText("Room number was already added.");
                alert.showAndWait();
            }

        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Room");
            alert.setHeaderText("");
            alert.setContentText("Please fill in all required information.");
            alert.showAndWait();
        }


    }


}