package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class OfficialController {

    @FXML Button officerBtn;
    @FXML Button administratorBtn;
    @FXML Button backBtn;

    @FXML public void handleOfficerOnAction(ActionEvent event) throws IOException {
        Button a = (Button) event.getSource();
        Stage stage_loginOfficer = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_officer.fxml"));
        stage_loginOfficer.setScene(new Scene(loader.load(),800,600));
        stage_loginOfficer.show();
    }

    @FXML public void handleAdministratorOnAction(ActionEvent event) throws IOException {
        Button a = (Button) event.getSource();
        Stage stage_loginAdministrator = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_administrator.fxml"));
        stage_loginAdministrator.setScene(new Scene(loader.load(),800,600));
        stage_loginAdministrator.show();
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button h = (Button) event.getSource();
        Stage stage = (Stage) h.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/homepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }
}
