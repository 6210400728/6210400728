package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Officer;
import model.Resident;
import model.Room;
import services.RoomData;
import services.RoomFileData;

import java.io.IOException;
import java.util.ArrayList;

public class ShowRoomController {
    @FXML Button backBtn;
    @FXML TableView<Room> roomTableView;
    @FXML Label officerLabel;

    private ObservableList<Room> roomObservableList;
    private Room room;
    private Officer officer;
    private RoomData roomData;
    private ArrayList<Room> roomList;
    private String selectData;
    public void setSelectData(String selectData) {
        this.selectData = selectData;
    }

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }


    public void initialize(){
        roomData = new RoomFileData("database","room.csv");
        roomList = roomData.getRoomData().getRoomList();
        Platform.runLater(new Runnable()
        {   @Override public void run(){
            officerLabel.setText("Account officer : "+officer.getUserName());
            showData();
        }
        });
    }

    public void showData(){
        roomList.forEach(room1 -> {
            Button viewBtn = room1.getViewBtn();
            viewBtn.setText("view");
            viewBtn.setOnAction((ActionEvent event) -> {
                room = room1;
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view_resident.fxml"));
                try {
                    stage.setScene(new Scene(loader.load(),800,600));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ViewResidentController view = loader.getController();
                view.setRoom(room);
                view.setOfficer(officer);
                stage.show();
            });
        });
        roomObservableList= FXCollections.observableArrayList(roomList);
        roomTableView.setItems(roomObservableList);

        TableColumn room = new TableColumn("Room number");
        TableColumn building = new TableColumn("Building");
        TableColumn floor = new TableColumn("Floor");
        TableColumn size = new TableColumn("Size");
        TableColumn button = new TableColumn("View");

        room.setCellValueFactory(new PropertyValueFactory<Resident,String>("numRoom"));
        building.setCellValueFactory(new PropertyValueFactory<Resident,String>("build"));
        floor.setCellValueFactory(new PropertyValueFactory<Resident,String>("floor"));
        size.setCellValueFactory(new PropertyValueFactory<Resident,String>("size"));
        button.setCellValueFactory(new PropertyValueFactory<>("viewBtn"));


        roomTableView.getColumns().addAll(room, building, floor, size,button);

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/select_data.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        SelectDataController view = loader.getController();
        view.setOfficer(officer);
        stage.show();
    }

}
