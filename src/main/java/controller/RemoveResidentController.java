package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;
import services.ResidentFileData;
import services.RoomFileData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class RemoveResidentController {
    private ObservableList<Resident> residentObservableList;
    private ResidentFileData residentFile;
    private ResidentList residentList;
    private Room room;
    private Resident selectResident;
    private Officer officer;
    private RoomList roomList;
    private RoomFileData roomData;

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @FXML TableView<Resident> officerTable;
    @FXML Button backBtn;
    @FXML Button removeBtn;
    @FXML Label officerLabel;

    @FXML public void initialize(){
        roomData = new RoomFileData("database","room.csv");
        roomList = roomData.getRoomData();
        Platform.runLater(new Runnable()
        {   @Override public void run(){
            officerLabel.setText("Account officer : "+officer.getUserName());
        }
        });
        residentFile = new ResidentFileData("database","resident.csv");
        residentList = residentFile.getResidentData();
        Platform.runLater(new Runnable()
        {   @Override public void run(){
            showData();
        }
        });
        officerTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{
            if (newValue != null) {
                selectResident = newValue;
            }
        });


    }

    @FXML public void handleRemoveOnAction(){
        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
        confirm.setTitle("Confirmation");
        confirm.setHeaderText("");
        confirm.setContentText("This action cannot be undone, do you want to continue?");
        Optional<ButtonType> result = confirm.showAndWait();

        residentList.remove(selectResident);
        roomList.addCount(room);
        roomData.setRoomData(roomList);
        residentFile.setResidentData(residentList.getResidents());
        showData();
    }


    public void showData(){
        officerTable.getColumns().clear();
        ArrayList<Resident> residentChoose = new ArrayList<>();
        for(Resident resident:residentList.getResidents()){
            if(resident.getRoom().equals(room.getNumRoom())){
                residentChoose.add(resident);
            }
        }


        residentObservableList= FXCollections.observableArrayList(residentChoose);
        officerTable.setItems(residentObservableList);

        TableColumn name = new TableColumn("Name");
        TableColumn surname = new TableColumn("Surname");
        TableColumn username = new TableColumn("Phone Number");

        name.setCellValueFactory(new PropertyValueFactory<Resident,String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<Resident,String>("surName"));
        username.setCellValueFactory(new PropertyValueFactory<Resident,String>("phone"));


        officerTable.getColumns().addAll(name,surname,username);

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        ChooseRoomController back = loader.getController();
        back.setOfficer(officer);
        back.setSelectData("removeInfoBtn");
        stage.show();

    }
}
