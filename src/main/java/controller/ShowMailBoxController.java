package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;
import services.LetterFileData;
import services.MailData;
import services.PaperFileData;
import services.ParcelFileData;

import java.io.IOException;

public class ShowMailBoxController {

    @FXML Button backBtn;
    @FXML Label officerLabel;
    @FXML TableView<Mail> mailboxTable;
    private ObservableList<Mail> mailObservableList;
    private MailData mailFile;
    private Mail mail ;
    private MailList mailList,exportList;
    private Mail papersList;
    private Mail parcelList;
    private Officer officer;
    private String selectType;
    private String selectData;
    private Resident current;
    public void setCurrent(Resident current){
        this.current = current;
    }


    public void setSelectData(String selectData) {

        this.selectData = selectData;
    }

    public void setSelectType(String selectType){

        this.selectType = selectType;
    }

    public void setOfficer(Officer officer) {

        this.officer = officer;
    }

    @FXML public void initialize() {

        Platform.runLater(() -> {
            if(selectType.equals("papersBtn")){
                mailFile = new PaperFileData("database", "papers.csv");
            }else if(selectType.equals("letterBtn")){
                mailFile = new LetterFileData("database", "letter.csv");
            }else if(selectType.equals("parcelBtn")){
                mailFile = new ParcelFileData("database", "parcel.csv");
            }
            mailList = mailFile.getMailData();//polymorphism
            officerLabel.setText("Account officer : " + officer.getUserName());
            showData();

        });

    }

    public void showData(){
        if(selectData.equals("viewBtn")){
            if(selectType.equals("papersBtn")){
                exportList = mailList.getExportItems();
                mailObservableList = FXCollections.observableArrayList(exportList.getMail());
                mailboxTable.setItems(mailObservableList);
                TableColumn date = new TableColumn("Date & Time");
                TableColumn room = new TableColumn("Room");
                TableColumn sender = new TableColumn("Sender");
                TableColumn receiver = new TableColumn("Receiver");
                TableColumn size = new TableColumn("Size");
                TableColumn important = new TableColumn("Priority");
                TableColumn status = new TableColumn("Status");
                TableColumn receiveBy = new TableColumn("Received");


                date.setCellValueFactory(new PropertyValueFactory<Papers,String>("time"));
                room.setCellValueFactory(new PropertyValueFactory<Papers,String>("room"));
                sender.setCellValueFactory(new PropertyValueFactory<Papers,String>("sender"));
                receiver.setCellValueFactory(new PropertyValueFactory<Papers,String>("receiver"));
                size.setCellValueFactory(new PropertyValueFactory<Papers,String>("size"));
                important.setCellValueFactory(new PropertyValueFactory<Papers,String>("important"));
                status.setCellValueFactory(new PropertyValueFactory<Papers,String>("status"));
                receiveBy.setCellValueFactory(new PropertyValueFactory<Papers,String>("receiveBy"));

                mailboxTable.getColumns().addAll(date, room,sender,receiver,size,important,status,receiveBy);
                date.setSortType(TableColumn.SortType.DESCENDING);
                mailboxTable.getSortOrder().add(date);
            }
            else if (selectType.equals("letterBtn")){
                exportList = mailList.getExportItems();
                mailObservableList = FXCollections.observableArrayList(exportList.getMail());
                mailboxTable.setItems(mailObservableList);
                TableColumn date = new TableColumn("Date & Time");
                TableColumn room = new TableColumn("Room");
                TableColumn sender = new TableColumn("Sender");
                TableColumn receiver = new TableColumn("Receiver");
                TableColumn size = new TableColumn("Size");
                TableColumn status = new TableColumn("Status");
                TableColumn receiveBy = new TableColumn("Received");

                date.setCellValueFactory(new PropertyValueFactory<Letter,String>("time"));
                room.setCellValueFactory(new PropertyValueFactory<Letter,String>("room"));
                sender.setCellValueFactory(new PropertyValueFactory<Letter,String>("sender"));
                receiver.setCellValueFactory(new PropertyValueFactory<Letter,String>("receiver"));
                size.setCellValueFactory(new PropertyValueFactory<Letter,String>("size"));
                status.setCellValueFactory(new PropertyValueFactory<Letter,String>("status"));
                receiveBy.setCellValueFactory(new PropertyValueFactory<Letter,String>("receiveBy"));


                mailboxTable.getColumns().addAll(date, room,sender,receiver,size, status, receiveBy);
                date.setSortType(TableColumn.SortType.DESCENDING);
                mailboxTable.getSortOrder().add(date);
            }
            else if (selectType.equals("parcelBtn")){
                exportList = mailList.getExportItems();
                mailObservableList = FXCollections.observableArrayList(exportList.getMail());
                mailboxTable.setItems(mailObservableList);
                TableColumn date = new TableColumn("Date & Time");
                TableColumn room = new TableColumn("Room");
                TableColumn sender = new TableColumn("Sender");
                TableColumn receiver = new TableColumn("Receiver");
                TableColumn size = new TableColumn("Size");
                TableColumn shipping = new TableColumn("Shipping");
                TableColumn tracking = new TableColumn("Tracking no.");
                TableColumn status = new TableColumn("Status");
                TableColumn receiveBy = new TableColumn("Received");

                date.setCellValueFactory(new PropertyValueFactory<Parcel,String>("time"));
                room.setCellValueFactory(new PropertyValueFactory<Parcel,String>("room"));
                sender.setCellValueFactory(new PropertyValueFactory<Parcel,String>("sender"));
                receiver.setCellValueFactory(new PropertyValueFactory<Parcel,String>("receiver"));
                size.setCellValueFactory(new PropertyValueFactory<Parcel,String>("size"));
                shipping.setCellValueFactory(new PropertyValueFactory<Parcel,String>("shipping"));
                tracking.setCellValueFactory(new PropertyValueFactory<Parcel,String>("tracking"));
                status.setCellValueFactory(new PropertyValueFactory<Parcel,String>("status"));
                receiveBy.setCellValueFactory(new PropertyValueFactory<Parcel,String>("receiveBy"));


                mailboxTable.getColumns().addAll(date, room,sender,receiver,size, shipping,tracking,status,receiveBy);
                date.setSortType(TableColumn.SortType.DESCENDING);
                mailboxTable.getSortOrder().add(date);
            }
        }

    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        if(selectData.equals("viewBtn")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_before_show.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            ChooseTypeBeforeShowController choose = loader.getController();
            choose.setOfficer(officer);
            choose.setSelectData(selectData);
            stage.show();
        }

    }



}
