package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import model.Officer;
import model.OfficerList;
import services.OfficerData;
import services.OfficerFileData;

import java.io.IOException;

public class ChangePasswordOfficerController {
    private Officer officer;
    private OfficerList officerList;
    private OfficerData officerData;
    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    @FXML PasswordField oldPasswordField;
    @FXML PasswordField newPasswordField;
    @FXML PasswordField confirmPasswordField;
    @FXML Button okBtn;
    @FXML Button backBtn;

    @FXML
    public void initialize() throws IOException {
        officerList = new OfficerList();
        officerData = new OfficerFileData("database", "officer.csv");
    }

    @FXML
    public void handleOkOnAction(ActionEvent event) throws IOException {
        OfficerList officers = officerData.getOfficerData();
        Officer findOfficer = null;
        boolean check = false;
        if(oldPasswordField.getText().isEmpty() || newPasswordField.getText().isEmpty() || confirmPasswordField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Password");
            alert.setHeaderText("");
            alert.setContentText("Please fill in all required password.");
            alert.showAndWait();
        }else {
            for (Officer officer1 : officers.getOfficersList()) {
                if (officer1.getPassword().equals(oldPasswordField.getText())) {
                    check = true;
                    findOfficer = officer1;
                    break;
                }

            }
            if (check) {
                if (newPasswordField.getText().equals(confirmPasswordField.getText())) {
                    findOfficer.setPassword(newPasswordField.getText());
                    officerData = new OfficerFileData("database", "officer.csv");
                    officerData.setOfficerData(officers);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Password");
                    alert.setHeaderText("");
                    alert.setContentText("Change password complete.");
                    alert.showAndWait();

                    Button o = (Button) event.getSource();
                    Stage stage_homepage = (Stage) o.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_officer.fxml"));
                    stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                    stage_homepage.show();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Password");
                    alert.setHeaderText("");
                    alert.setContentText("Password is not correct.");
                    newPasswordField.setText("");
                    confirmPasswordField.setText("");
                    alert.showAndWait();
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Password");
                alert.setHeaderText("");
                alert.setContentText("Password is not correct.");
                oldPasswordField.setText("");
                alert.showAndWait();
            }

        }

}



    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_officer.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();

    }

    }
