package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Officer;
import model.OfficerList;
import services.OfficerData;
import services.OfficerFileData;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LoginForOfficerController{
    @FXML TextField userField;
    @FXML TextField passwordField;
    @FXML Button okBtn;
    @FXML Button backBtn;
    private OfficerData officerData;
    private OfficerList officerList;

    public void initialize() {
        officerList = new OfficerList();
        officerData = new OfficerFileData("database", "officer.csv");
        officerList = officerData.getOfficerData();
    }

    private Officer current;

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {
        current = officerList.checkAndGetOfficer(userField.getText(), passwordField.getText());
        if(current != null) {
            String date = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
            current.setTime(date);
            officerData.setOfficerData(officerList);
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/select_data.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            SelectDataController selectData = loader.getController();
            selectData.setOfficer(current);
            stage.show();
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Officer");
            alert.setHeaderText("");
            alert.setContentText("Incorrect username or password");
            passwordField.setText("");
            userField.setText("");
            alert.showAndWait();
        }
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/official.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();

    }
}
