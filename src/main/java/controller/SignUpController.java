package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Resident;
import model.ResidentList;
import services.ResidentFileData;
import services.RoomFileData;

import java.io.IOException;
import java.util.ArrayList;

public class SignUpController {
    private ResidentFileData residentData;
    private ResidentList residentList;
    private Resident resident;
    @FXML TextField nameField;
    @FXML TextField sureNameField;
    @FXML TextField phoneField;
    @FXML TextField userField;
    @FXML PasswordField passwordField;
    @FXML PasswordField confirmField;
    @FXML ComboBox<String> build;
    @FXML ComboBox<String> floor;
    @FXML ComboBox<String> size;
    @FXML ComboBox<String> roomNum;

    public void initialize(){

        build.getItems().addAll("A", "B");
        floor.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12");
        size.getItems().addAll("30 Square meters","40 Square meters","55 Square meters");
        roomNum.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12","12A","14","15","16");
        residentData = new ResidentFileData("database","resident.csv");
        residentList = residentData.getResidentData();
    }


    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {
            Boolean checkRoom = false;
            for (Resident resident:residentList.getResidents()) {
                if (resident.checkResident(nameField.getText(), sureNameField.getText(), phoneField.getText(), build.getValue() + floor.getValue() + roomNum.getValue())) {
                    checkRoom = true;
                    this.resident=resident;
                    break;
                }
            }
            if(checkRoom){
                if(resident.getUserName().equals("-")) {
                    boolean checkUsername = true;
                    for (Resident resident : residentList.getResidents()) {
                        if (resident.getUserName().equals(userField.getText())) {
                            checkUsername = false;
                            break;
                        }
                    }
                    if (checkUsername) {
                        if (passwordField.getText().equals(confirmField.getText())) {
                            resident.setUserName(userField.getText());
                            resident.setPassword(passwordField.getText());
                            residentData.setResidentData(residentList.getResidents());
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Sign up");
                            alert.setHeaderText("");
                            alert.setContentText("Create username complete.");
                            alert.showAndWait();

                            javafx.scene.control.Button n = (javafx.scene.control.Button) event.getSource();
                            Stage stage = (Stage) n.getScene().getWindow();
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/homepage.fxml"));
                            stage.setScene(new Scene(loader.load(), 800, 600));
                            stage.show();
                        } else if (!(passwordField.getText().equals(confirmField.getText()))) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Sign up");
                            alert.setHeaderText("");
                            alert.setContentText("Password is not correct.");
                            passwordField.setText("");
                            confirmField.setText("");
                            alert.showAndWait();
                        }
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Sign up");
                        alert.setHeaderText("");
                        alert.setContentText("Username is already used.");
                        userField.setText("");
                        alert.showAndWait();
                    }
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Sign up");
                    alert.setHeaderText("");
                    alert.setContentText("Your already have account.");
                    userField.setText("");
                    alert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Sign up");
                alert.setHeaderText("");
                alert.setContentText("Information is not correct.");
                alert.showAndWait();
            }

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/homepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

}
