package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Officer;
import model.Resident;
import model.ResidentList;
import services.OfficerData;
import services.OfficerFileData;
import services.ResidentData;
import services.ResidentFileData;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class LoginController {
    @FXML TextField userField;
    @FXML PasswordField passwordField;
    private ResidentData residentData;
    private Resident resident;
    private ResidentList residentList;

    public void initialize() {
        residentData = new ResidentFileData("database", "resident.csv");
        residentList = residentData.getResidentData();
    }
    private Resident current;

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {

        current=null;
        for (Resident resident : residentList.room1()) {
            if (userField.getText().equals(resident.getUserName()) && passwordField.getText().equals(resident.getPassword())) {
                current=resident;
            }
        }

        if(current != null) {

            residentData.setResidentData(residentList.room1());
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_before_show_resident.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            ChooseTypeBeforeShowResidentController resident = loader.getController();
            resident.setCurrent(current);
            stage.show();
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Officer");
            alert.setHeaderText("");
            alert.setContentText("Incorrect username or password");
            passwordField.setText("");
            userField.setText("");
            alert.showAndWait();
        }
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/homepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();

    }

}
