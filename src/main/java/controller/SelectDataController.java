package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Officer;
import model.OfficerList;
import services.OfficerData;
import services.OfficerFileData;

import java.io.IOException;

public class SelectDataController {
    @FXML Button addInfoBtn;
    @FXML Button removeInfoBtn;
    @FXML Button viewInfoBtn;
    @FXML Button addParcelBtn;
    @FXML Button removeParcelBtn;
    @FXML Button viewParcelBtn;
    @FXML Button backBtn;
    @FXML Button RoomBtn;
    @FXML Label officerLabel;
    @FXML Button changePasswordBtn;

    private Officer officer;
    private OfficerList officerList;
    private OfficerData officerData;


    private String select = "";

    @FXML public void initialize(){
        Platform.runLater(new Runnable()
        {   @Override public void run(){
                officerLabel.setText("Account officer : "+officer.getUserName());
            }
        });
    }

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    @FXML public void handleAddInfoOnAction(ActionEvent event) throws IOException {
        Button information = (Button) event.getSource();
        Stage stage = (Stage) information.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        select = "addInfoBtn";
        ChooseRoomController selectData = loader.getController();
        selectData.setSelectData(select);
        selectData.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleRemoveInfoOnAction(ActionEvent event) throws IOException {
        Button information = (Button) event.getSource();
        Stage stage = (Stage) information.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        select = "removeInfoBtn";
        ChooseRoomController selectData = loader.getController();
        selectData.setSelectData(select);
        selectData.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleViewInfoOnAction(ActionEvent event) throws IOException {
        Button parcel = (Button) event.getSource();
        Stage stage = (Stage) parcel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_room.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        select = "viewInfoBtn";
        ShowRoomController selectData = loader.getController();
        selectData.setSelectData(select);
        selectData.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleImportOnAction(ActionEvent event) throws IOException {
        Button parcel = (Button) event.getSource();
        Stage stage = (Stage) parcel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_mailbox.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        select = "importBtn";
        ChooseTypeMailBoxController selectData = loader.getController();
        selectData.setSelectData(select);
        selectData.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleExportOnAction(ActionEvent event) throws IOException {
        Button parcel = (Button) event.getSource();
        Stage stage = (Stage) parcel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_before_show.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        select = "exportBtn";
        ChooseTypeBeforeShowController selectData = loader.getController();
        selectData.setSelectData(select);
        selectData.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleViewOnAction(ActionEvent event) throws IOException {
        Button parcel = (Button) event.getSource();
        Stage stage = (Stage) parcel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_before_show.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        select = "viewBtn";
        ChooseTypeBeforeShowController selectData = loader.getController();
        selectData.setSelectData(select);
        selectData.setOfficer(officer);
        stage.show();
    }


    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        officerData = new OfficerFileData("database", "officer.csv");
        officerList = officerData.getOfficerData();
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_officer.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        LoginForOfficerController add = loader.getController();
        stage.show();
    }

    @FXML public void handleRoomOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/add_room.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddRoomController add = loader.getController();
        add.setOfficer(officer);
        stage.show();
    }

    @FXML public void handleChangePasswordOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_password_officer.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ChangePasswordOfficerController change = loader.getController();
        change.setOfficer(officer);
        stage.show();
    }



}
