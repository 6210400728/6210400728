package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Administrator;

import java.io.*;

public class LoginForAdministratorController {
    private Administrator admin;
    @FXML TextField userField;
    @FXML PasswordField passwordField;
    @FXML Button okBtn;
    @FXML Button backBtn;
    @FXML Button changePasswordBtn;

    @FXML public void initialize() {
        try {
            FileReader inReader = new FileReader("database/admin.csv");
            BufferedReader buffer = new BufferedReader(inReader);
            String line;

            while ((line = buffer.readLine()) != null){
                String[] data = line.split(",");
                admin = new Administrator(data[0], data[1]);
            }
            buffer.close();
            inReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/official.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {
        if (admin.check(userField.getText(), passwordField.getText())) {
            Button o = (Button) event.getSource();
            Stage stage = (Stage) o.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_for_admin.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            stage.show();
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Administrator");
            alert.setHeaderText("");
            alert.setContentText("Please enter the information again.");
            passwordField.setText("");
            userField.setText("");
            alert.showAndWait();
        }

    }

    @FXML public void handleChangePasswordOnAction(ActionEvent event) throws IOException {
        Button c = (Button) event.getSource();
        Stage stage = (Stage) c.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_password_admin.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
}
