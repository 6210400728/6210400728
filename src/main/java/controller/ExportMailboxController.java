package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;
import services.LetterFileData;
import services.MailData;
import services.PaperFileData;
import services.ParcelFileData;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;

public class ExportMailboxController {
    private Officer officer;
    private Room room;
    private ObservableList<Mail> mailObservableList;
    private MailData mailFile;
    private Mail mail;
    private MailList mailList, importList;
    private String selectType;
    private String selectData;

    public void setSelectData(String selectData) {
        this.selectData = selectData;
    }

    public void setSelectType(String selectType){

        this.selectType = selectType;
    }

    public void setOfficer(Officer officer) {

        this.officer = officer;
    }

    public void setRoom(Room room) {

        this.room = room;
    }

    @FXML TableView<Mail> mailBoxTable;
    @FXML Button backBtn;
    @FXML Button exportBtn;
    @FXML Label officerLabel;
    @FXML TextField receiverByTextField;

    @FXML public void initialize() {

        Platform.runLater(() -> {
            if(selectType.equals("papersBtn")){
                mailFile = new PaperFileData("database", "papers.csv");
            }else if(selectType.equals("letterBtn")){
                mailFile = new LetterFileData("database", "letter.csv");
            }else if(selectType.equals("parcelBtn")){
                mailFile = new ParcelFileData("database", "parcel.csv");
            }
            mailList = mailFile.getMailData();
            officerLabel.setText("Account officer : " + officer.getUserName());
            showData();

        });
        mailBoxTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                mail = newValue;
            }
        });
    }


    @FXML public void handleExportOnAction(ActionEvent event) throws IOException {
        if(receiverByTextField.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Empty Field Detected");
            alert.setHeaderText("");
            alert.setContentText("Please enter receiver.");
            alert.showAndWait();
        }
        else {
            Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
            confirm.setTitle("Confirmation");
            confirm.setHeaderText("");
            confirm.setContentText("This action cannot be undone, do you want to continue?");
            Optional<ButtonType> result = confirm.showAndWait();


            if(result.get() == ButtonType.OK){
                String date = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
                mail.setStatus("export");
                mail.setTime(date);
                mail.setReceiveBy(receiverByTextField.getText());
                mailFile.setMailData(mailList);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("");
                alert.setContentText("Received successfully.");
                alert.showAndWait();

                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/export_mailbox.fxml"));
                stage.setScene(new Scene(loader.load(),800,600));
                ExportMailboxController export = loader.getController();
                export.setOfficer(officer);
                export.setSelectData(selectData);
                export.setSelectType(selectType);
                stage.show();
            }

        }

    }

    public void showData(){
        if(selectData.equals("exportBtn")){
            if(selectType.equals("papersBtn")){
                importList = mailList.getImportItems();
                mailObservableList = FXCollections.observableArrayList(importList.getMail());
                mailBoxTable.setItems(mailObservableList);
                TableColumn date = new TableColumn("Date & Time");
                TableColumn room = new TableColumn("Room");
                TableColumn sender = new TableColumn("Sender");
                TableColumn receiver = new TableColumn("Receiver");
                TableColumn size = new TableColumn("Size");
                TableColumn important = new TableColumn("Priority");

                date.setCellValueFactory(new PropertyValueFactory<Papers,String>("time"));
                room.setCellValueFactory(new PropertyValueFactory<Papers,String>("room"));
                sender.setCellValueFactory(new PropertyValueFactory<Papers,String>("sender"));
                receiver.setCellValueFactory(new PropertyValueFactory<Papers,String>("receiver"));
                size.setCellValueFactory(new PropertyValueFactory<Papers,String>("size"));
                important.setCellValueFactory(new PropertyValueFactory<Papers,String>("important"));

                mailBoxTable.getColumns().addAll(date,room, sender,receiver,size,important);
                date.setSortType(TableColumn.SortType.DESCENDING);
                mailBoxTable.getSortOrder().add(date);
            }
            else if (selectType.equals("letterBtn")){
                importList = mailList.getImportItems();
                mailObservableList = FXCollections.observableArrayList(importList.getMail());
                mailBoxTable.setItems(mailObservableList);
                TableColumn date = new TableColumn("Date & Time");
                TableColumn room = new TableColumn("Room");
                TableColumn sender = new TableColumn("Sender");
                TableColumn receiver = new TableColumn("Receiver");
                TableColumn size = new TableColumn("Size");

                date.setCellValueFactory(new PropertyValueFactory<Letter,String>("time"));
                room.setCellValueFactory(new PropertyValueFactory<Letter,String>("room"));
                sender.setCellValueFactory(new PropertyValueFactory<Letter,String>("sender"));
                receiver.setCellValueFactory(new PropertyValueFactory<Letter,String>("receiver"));
                size.setCellValueFactory(new PropertyValueFactory<Letter,String>("size"));


                mailBoxTable.getColumns().addAll(date,room, sender,receiver,size);
                date.setSortType(TableColumn.SortType.DESCENDING);
                mailBoxTable.getSortOrder().add(date);
            }
            else if (selectType.equals("parcelBtn")){
                importList = mailList.getImportItems();
                mailObservableList = FXCollections.observableArrayList(importList.getMail());
                mailBoxTable.setItems(mailObservableList);
                TableColumn date = new TableColumn("Date & Time");
                TableColumn room = new TableColumn("Room");
                TableColumn sender = new TableColumn("Sender");
                TableColumn receiver = new TableColumn("Receiver");
                TableColumn size = new TableColumn("Size");
                TableColumn shipping = new TableColumn("Shipping");
                TableColumn tracking = new TableColumn("Tracking no.");

                date.setCellValueFactory(new PropertyValueFactory<Parcel,String>("time"));
                room.setCellValueFactory(new PropertyValueFactory<Parcel,String>("room"));
                sender.setCellValueFactory(new PropertyValueFactory<Parcel,String>("sender"));
                receiver.setCellValueFactory(new PropertyValueFactory<Parcel,String>("receiver"));
                size.setCellValueFactory(new PropertyValueFactory<Parcel,String>("size"));
                shipping.setCellValueFactory(new PropertyValueFactory<Parcel,String>("shipping"));
                tracking.setCellValueFactory(new PropertyValueFactory<Parcel,String>("tracking"));


                mailBoxTable.getColumns().addAll(date,room, sender,receiver,size, shipping,tracking);
                date.setSortType(TableColumn.SortType.DESCENDING);
                mailBoxTable.getSortOrder().add(date);
            }
        }

    }


    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type_before_show.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        ChooseTypeBeforeShowController choose = loader.getController();
        choose.setOfficer(officer);
        choose.setSelectData(selectData);
        stage.show();
    }

}
