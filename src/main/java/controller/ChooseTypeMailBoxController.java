package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.*;
import services.ResidentFileData;
import services.RoomFileData;

import java.io.IOException;
import java.util.ArrayList;

public class ChooseTypeMailBoxController {

    @FXML
    Label officerLabel;
    @FXML
    MenuButton type;
    @FXML
    MenuItem letterItem;
    @FXML
    MenuItem paperItem;
    @FXML
    MenuItem parcelItem;
    @FXML
    Button nextBtn;
    @FXML
    Button backBtn;
    @FXML ComboBox<String> chooseRoom;

    private Officer officer;
    private String selectType = "";
    private String selectData;
    private String room;
    private int count = 0;
    private RoomList roomList;
    private ResidentList residentList;
    private ResidentFileData residentData;
    private Resident resident;

    public void setSelectData(String selectType) {
        this.selectData = selectData;
    }

    public void setSelectType(String selectType) {
        this.selectType = selectType;
    }

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    @FXML
    public void initialize() {
        ResidentFileData residentData = new ResidentFileData("database","resident.csv");
        residentList = residentData.getResidentData();

            ArrayList<String> roomNum = new ArrayList<>();
            ArrayList<String> newList = new ArrayList<>();
            for(Resident resident : residentList.getResidents()){
                roomNum.add(resident.getRoom());
            }
            for(String room : roomNum){
                if(!newList.contains(room)){
                    newList.add(room);
                }
            }
            for (String num : newList){
                chooseRoom.getItems().add(num);
            }




        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                officerLabel.setText("Account officer : " + officer.getUserName());
            }
        });
    }

    @FXML
    public void handleNextOnAction(ActionEvent event) throws IOException {
        RoomFileData roomData = new RoomFileData("database", "room.csv");
        RoomList rooms = roomData.getRoomData();
        Room findRoom = null;
        boolean check = false;
        for (Room room : rooms.getRoomList()) {
            if (room.getNumRoom().equals(chooseRoom.getValue())) {
                check = true;
                findRoom = room;
                break;
            }
        }
        if (check) {
            Button ButtonAboutMe = (Button) event.getSource();
            Stage stage = (Stage) ButtonAboutMe.getScene().getWindow();
            String type = null;
            if (selectType.equals("")) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Type");
                alert.setHeaderText("");
                alert.setContentText("Please choose type.");
                alert.showAndWait();
            } else {
                if (selectType.equals("letterItem")) {
                    type = "letter";
                }
                else if (selectType.equals("paperItem")) {
                    type = "paper";
                }
                else if (selectType.equals("parcelItem")) {
                    type = "parcel";
                }
                Button h = (Button) event.getSource();
                Stage stage_homepage = (Stage) h.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/add_parcel.fxml"));
                stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                AddMailController add = loader.getController();
                add.setRoom(findRoom);
                add.setOfficer(officer);
                add.setType(type);
                stage_homepage.show();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Room");
            alert.setHeaderText("");
            alert.setContentText("Room not found.");
            alert.showAndWait();
        }

    }

    @FXML public void handleLetterOnAction(ActionEvent event)throws IOException{
        MenuItem menuItemLetter = (MenuItem) event.getSource();
        selectType = menuItemLetter.getId();
        type.setText(menuItemLetter.getText());
    }

    @FXML public void handlePaperOnAction(ActionEvent event)throws IOException{
        MenuItem menuItemPaper = (MenuItem) event.getSource();
        selectType = menuItemPaper.getId();
        type.setText(menuItemPaper.getText());
    }

    @FXML public void handleParcelOnAction(ActionEvent event)throws IOException{
        MenuItem menuItemParcel = (MenuItem) event.getSource();
        selectType = menuItemParcel.getId();
        type.setText(menuItemParcel.getText());
    }


    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {

        Button h = (Button) event.getSource();
        Stage stage_homepage = (Stage) h.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/select_data.fxml"));
        stage_homepage.setScene(new Scene(loader.load(),800,600));
        SelectDataController back = loader.getController();
        back.setOfficer(officer);
        stage_homepage.show();
    }



}
