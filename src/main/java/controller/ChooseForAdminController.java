package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class ChooseForAdminController {
    @FXML Button RegisterBtn;
    @FXML Button ViewBtn;
    @FXML Button backBtn;

    @FXML public void handleRegOnAction(ActionEvent event) throws IOException {
        Button a = (Button) event.getSource();
        Stage stage_loginOfficer = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sign_up_for_officer.fxml"));
        stage_loginOfficer.setScene(new Scene(loader.load(),800,600));
        stage_loginOfficer.show();
    }

    @FXML public void handleViewOnAction(ActionEvent event) throws IOException {
        Button a = (Button) event.getSource();
        Stage stage_loginOfficer = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_time_officer.fxml"));
        stage_loginOfficer.setScene(new Scene(loader.load(),800,600));
        stage_loginOfficer.show();
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button a = (Button) event.getSource();
        Stage stage_loginOfficer = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_for_administrator.fxml"));
        stage_loginOfficer.setScene(new Scene(loader.load(),800,600));
        stage_loginOfficer.show();
    }
}
