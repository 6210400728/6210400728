package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.*;
import services.ResidentFileData;
import services.RoomFileData;

import java.io.IOException;
import java.util.ArrayList;

public class AddResidentController {
    private Room room;
    private ResidentFileData residentData;
    private ResidentList residentList;
    private Officer officer;
    private RoomList roomList;
    private RoomFileData roomData;
    @FXML Label amountLabel;
    @FXML Label addedLabel;
    @FXML TextField nameTextField;
    @FXML TextField surnameTextField;
    @FXML TextField phoneTextField;
    @FXML Label officerLabel;


    public void setOfficer(Officer officer) {
        this.officer = officer;
    }

    public void setRoom(Room room) {
        this.room = room;
    }


    @FXML public void initialize(){
        roomData = new RoomFileData("database","room.csv");
        roomList = roomData.getRoomData();
        Platform.runLater(new Runnable()
        {   @Override public void run(){
            officerLabel.setText("Account officer : "+officer.getUserName());
        }
        });

        residentData = new ResidentFileData("database","resident.csv");
        residentList = residentData.getResidentData();
        Platform.runLater(new Runnable()
            {   @Override public void run(){

                    int count = 0;
                    for(Resident resident:residentList.getResidents()){
                        if(resident.getRoom().equals(room.getRoom())){
                            count+=1;
                        }
                    }
                    if(room.getSize().equals("30 Square meters")){
                        amountLabel.setText("2");
                        addedLabel.setText(String.valueOf(count));
                    }
                    else if(room.getSize().equals("40 Square meters")){
                        amountLabel.setText("3");
                        addedLabel.setText(String.valueOf(count));
                    }
                    else if(room.getSize().equals("55 Square meters")){
                        amountLabel.setText("5");
                        addedLabel.setText(String.valueOf(count));
                    }
                }

            });


    }
    @FXML public void handleNextOnAction(ActionEvent event) throws IOException {
        Room findRoom = room;

        if(room.getSize().equals("30 Square meters")) {
            if (addedLabel.getText().equals("2")) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Room");
                alert.setHeaderText("");
                alert.setContentText("Maximum resident in this room.");
                alert.showAndWait();
            } else {
                    if(nameTextField.getText().equals("") || surnameTextField.getText().equals("") ||  phoneTextField.getText().equals("")){
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Room");
                        alert.setHeaderText("");
                        alert.setContentText("Please fill in all required information.");
                        alert.showAndWait();

                    }
                    else {
                        Resident newResident = new Resident(nameTextField.getText(), surnameTextField.getText(), phoneTextField.getText()
                                , room.getBuild(), room.getFloor(), room.getNumRoom(), room.getSize(), "-", "-");
                        residentList.add(newResident);
                        roomList.deleteCount(room);
                        roomData.setRoomData(roomList);
                        residentData.setResidentData(residentList.getResidents());

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Room");
                        alert.setHeaderText("");
                        alert.setContentText("Create resident complete.");
                        alert.showAndWait();

                        Button n = (Button) event.getSource();
                        Stage stage = (Stage) n.getScene().getWindow();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
                        stage.setScene(new Scene(loader.load(), 800, 600));
                        ChooseRoomController add = loader.getController();
                        add.setOfficer(officer);
                        add.setSelectData("addInfoBtn");
                        stage.show();
                    }

            }
        }
        else if(room.getSize().equals("40 Square meters")){
            if(addedLabel.getText().equals("3")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Room");
                alert.setHeaderText("");
                alert.setContentText("Maximum resident in this room.");
                alert.showAndWait();
            }
            else {
                if(nameTextField.getText().equals("") || surnameTextField.getText().equals("") ||  phoneTextField.getText().equals("")){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Room");
                    alert.setHeaderText("");
                    alert.setContentText("Please fill in all required information.");
                    alert.showAndWait();
                }
                else {
                    Resident newResident = new Resident(nameTextField.getText(),surnameTextField.getText(),phoneTextField.getText()
                            ,room.getBuild(),room.getFloor(),room.getNumRoom(),room.getSize(),"-","-");
                    residentList.add(newResident);
                    roomList.deleteCount(room);
                    roomData.setRoomData(roomList);
                    residentData.setResidentData(residentList.getResidents());


                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Room");
                    alert.setHeaderText("");
                    alert.setContentText("Create resident complete.");
                    alert.showAndWait();

                    Button n = (Button) event.getSource();
                    Stage stage = (Stage) n.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
                    stage.setScene(new Scene(loader.load(),800,600));
                    ChooseRoomController add = loader.getController();
                    add.setOfficer(officer);
                    add.setSelectData("addInfoBtn");
                    stage.show();
                }


            }
        }
        else if(room.getSize().equals("55 Square meters")){
            if(addedLabel.getText().equals("5")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Room");
                alert.setHeaderText("");
                alert.setContentText("Maximum resident in this room.");
                alert.showAndWait();
            }
            else {
                if(nameTextField.getText().equals("") || surnameTextField.getText().equals("") ||  phoneTextField.getText().equals("")){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Room");
                    alert.setHeaderText("");
                    alert.setContentText("Please fill in all required information.");
                    alert.showAndWait();
                }
                else {
                    Resident newResident = new Resident(nameTextField.getText(),surnameTextField.getText(),phoneTextField.getText()
                            ,room.getBuild(),room.getFloor(),room.getNumRoom(),room.getSize(),"-","-");
                    residentList.add(newResident);
                    roomList.deleteCount(room);
                    roomData.setRoomData(roomList);
                    residentData.setResidentData(residentList.getResidents());

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Room");
                    alert.setHeaderText("");
                    alert.setContentText("Create resident complete.");
                    alert.showAndWait();

                    Button n = (Button) event.getSource();
                    Stage stage = (Stage) n.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
                    stage.setScene(new Scene(loader.load(),800,600));
                    ChooseRoomController add = loader.getController();
                    add.setOfficer(officer);
                    add.setSelectData("addInfoBtn");
                    stage.show();

                }

            }
        }

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_room.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        ChooseRoomController back = loader.getController();
        back.setOfficer(officer);
        back.setSelectData("addInfoBtn");
        stage.show();

    }


}
