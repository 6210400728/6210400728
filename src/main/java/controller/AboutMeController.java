package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class AboutMeController {
    @FXML Button homepageBtn;

    @FXML public void handleHomepageOnAction(ActionEvent event) throws IOException {
        Button h = (Button) event.getSource();
        Stage stage_homepage = (Stage) h.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/homepage.fxml"));
        stage_homepage.setScene(new Scene(loader.load(),800,600));
        stage_homepage.show();
    }
}
