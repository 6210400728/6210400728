package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Officer;
import model.OfficerList;
import services.OfficerData;
import services.OfficerFileData;

import java.io.*;

public class SignUpForOfficerController {
    @FXML TextField nameField;
    @FXML TextField sureNameField;
    @FXML TextField phoneField;
    @FXML TextField userField;
    @FXML TextField passwordField;
    @FXML TextField confirmField;
    @FXML Button okBtn;
    @FXML Button backBtn;

    private OfficerData officerData;
    private OfficerList officerList;

    public void initialize(){
        officerList = new OfficerList();
        officerData = new OfficerFileData("database","officer.csv");
        officerList = officerData.getOfficerData();
    }

    @FXML public void handleOkOnAction(ActionEvent event) throws IOException {
        Officer o = null;
        boolean check = true;
        for(Officer officer : officerList.getOfficersList()) {
            if(userField.getText().equals("")) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Administrator");
                alert.setHeaderText("No username");
                alert.setContentText("Please enter username.");
                userField.setText("");
                alert.showAndWait();
                check = false;
                break;
            }
            else if(userField.getText().equals(officer.getUserName())){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Administrator");
                alert.setHeaderText("This username has already been used.");
                alert.setContentText("Please enter username again.");
                userField.setText("");
                alert.showAndWait();
                check = false;
                break;
            }

        }
        if(check){
            if(passwordField.getText().equals(confirmField.getText())){
                Officer officer = new Officer(nameField.getText(),sureNameField.getText(),phoneField.getText(), userField.getText(),
                        passwordField.getText(), "-");
                officerList.addOfficer(officer);
                officerData = new OfficerFileData("database","officer.csv");
                officerData.setOfficerData(officerList);
                System.out.println(officerList);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Administrator");
                alert.setHeaderText("");
                alert.setContentText("Create username done.");
                alert.showAndWait();
                Button a = (Button) event.getSource();
                Stage stage_loginOfficer = (Stage) a.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sign_up_for_officer.fxml"));
                stage_loginOfficer.setScene(new Scene(loader.load(),800,600));
                stage_loginOfficer.show();
            }
            else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Administrator");
                alert.setHeaderText("");
                alert.setContentText("Please enter password again.");
                passwordField.setText("");
                confirmField.setText("");
                alert.showAndWait();
            }
        }



    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_for_admin.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

}
