package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import model.Resident;

import java.io.IOException;

public class ChooseTypeBeforeShowResidentController {
    private String selectType;
    private Resident current;
    public void setCurrent(Resident current){
        this.current = current;
    }

    public void setSelectType(String selectType) {
        this.selectType = selectType;
    }

    @FXML
    public void handlePapersOnAction(ActionEvent event) throws IOException {
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mail_for_resident.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "papersBtn";
            ShowMailResident papers = loader.getController();
            papers.setSelectType(selectType);
            papers.setCurrent(current);
            stage.show();

    }

    @FXML public void handleLetterOnAction(ActionEvent event) throws IOException {
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mail_for_resident.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "letterBtn";
            ShowMailResident letter = loader.getController();
            letter.setSelectType(selectType);
            letter.setCurrent(current);
            stage.show();

    }

    @FXML public void handleParcelOnAction(ActionEvent event) throws IOException {
            Button information = (Button) event.getSource();
            Stage stage = (Stage) information.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mail_for_resident.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            selectType = "parcelBtn";
            ShowMailResident parcel = loader.getController();
            parcel.setSelectType(selectType);
            parcel.setCurrent(current);
            stage.show();

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
            Button b1 = (Button) event.getSource();
            Stage stage = (Stage) b1.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));
            stage.show();
    }


}
