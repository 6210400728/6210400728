package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.*;
import services.ResidentFileData;
import services.RoomFileData;

import java.io.IOException;
import java.util.ArrayList;

public class ChooseRoomController {
    private String selectData;
    @FXML Button nextBtn;
    @FXML Button backBtn;
    @FXML TextField roomField;
    @FXML Label officerLabel;
    @FXML ComboBox<String> chooseRoom;

    private Officer officer;

    public void setSelectData(String selectData) {
        this.selectData = selectData;
    }

    public void setOfficer(Officer officer) {
        this.officer = officer;
    }


    @FXML public void initialize() {
        RoomFileData roomData = new RoomFileData("database", "room.csv");
        RoomList rooms = roomData.getRoomData();
        ResidentFileData residentData = new ResidentFileData("database", "resident.csv");
        ResidentList residents = residentData.getResidentData();
        Platform.runLater(() -> {
            if (selectData.equals("addInfoBtn")) {
                for (Room room1 : rooms.getRoomList()) {
                    if (room1.getCountResident() > 0) {
                        chooseRoom.getItems().addAll(room1.getNumRoom());
                    }
                }
            } else if(selectData.equals("removeInfoBtn")) {
                for (Room room1 : rooms.getRoomList()) {
                        chooseRoom.getItems().addAll(room1.getNumRoom());
                    }
            }

            officerLabel.setText("Account officer : " + officer.getUserName());
        });

    }



    @FXML public void handleNextOnAction(ActionEvent event) throws IOException {
        RoomFileData roomData = new RoomFileData("database","room.csv");
        RoomList rooms = roomData.getRoomData();
        Room findRoom = null;
        boolean check = false;
        for(Room room:rooms.getRoomList()){
            if(room.getNumRoom().equals(chooseRoom.getValue())){
                check = true;
                findRoom = room;
                break;
            }
        }
        if(check){
            if(selectData.equals("addInfoBtn")) {
                Button h = (Button) event.getSource();
                Stage stage_homepage = (Stage) h.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/add_resident.fxml"));
                stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                AddResidentController add = loader.getController();
                add.setRoom(findRoom);
                add.setOfficer(officer);
                stage_homepage.show();
            }
            else if(selectData.equals("removeInfoBtn")){
                Button h = (Button) event.getSource();
                Stage stage_homepage = (Stage) h.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/remove_resident.fxml"));
                stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                RemoveResidentController remove = loader.getController();
                remove.setRoom(findRoom);
                remove.setOfficer(officer);
                stage_homepage.show();
            }
            else if(selectData.equals("viewInfoBtn")){
                Button h = (Button) event.getSource();
                Stage stage_homepage = (Stage) h.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view_resident.fxml"));
                stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                ViewResidentController add = loader.getController();
                add.setRoom(findRoom);
                add.setOfficer(officer);
                stage_homepage.show();
            }
            else if(selectData.equals("viewBtn")){
                Button h = (Button) event.getSource();
                Stage stage_homepage = (Stage) h.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/show_mailbox.fxml"));
                stage_homepage.setScene(new Scene(loader.load(), 800, 600));
                AddResidentController add = loader.getController();
                add.setRoom(findRoom);
                stage_homepage.show();

                }
            }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Room");
            alert.setHeaderText("");
            alert.setContentText("Room not found.");
            roomField.setText("");
            alert.showAndWait();
        }

    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {

        Button h = (Button) event.getSource();
        Stage stage_homepage = (Stage) h.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/select_data.fxml"));
        stage_homepage.setScene(new Scene(loader.load(),800,600));
        SelectDataController back = loader.getController();
        back.setOfficer(officer);
        stage_homepage.show();
    }

}
