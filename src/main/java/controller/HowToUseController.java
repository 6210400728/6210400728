package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HowToUseController {

    public void start(Stage stage) {
        Text text = new Text();
        text.setText("คลิก menu เพื่อเลือก 4 ตัวเลือก สำหรับไปหน้าถัดไป ได้แก่ \n"+
                "1) Longin -> สำหรับลูกบ้านที่ได้ทำการลงทะเบียนแล้วเพื่อดูข้อมูล mail box ของห้องตัวเอง\n"+
                "2) Sign up -> สำหรับลูกบ้านที่มีชื่ออยู่ในห้องพัก ลงทะเบียนเพื่อสมัคร username และ password เพื่อดูข้อมูลของ mail box ตามห้อง\n"+
                "3) Official -> สำหรับเจ้าหน้าที่ส่วนกลางและผู้ดูและระบบ เพื่อเข้าไปจัดการกับข้อมูลของลูกบ้านและการรับส่งพัสดุ\n"+
                "4) About Me -> หน้าแสดงข้อมูลผู้จัดทำโปรแกรม ประกอบด้วย รูปที่แสดงหน้าชัดเจน, ชื่อ, นามสกุล, ชื่อเล่น, รหัสนิสิต\n"+
                "      เมื่อเลือกเมนูอย่างใดอย่างหนึ่ง ให้กดปุ่ม “confirm” เพื่อแสดงไปยังหน้าถัดไป         ");
        Group root = new Group(text);
        Scene scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button h = (Button) event.getSource();
        Stage stage_homepage = (Stage) h.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/homepage.fxml"));
        stage_homepage.setScene(new Scene(loader.load(),800,600));
        stage_homepage.show();
    }





}
